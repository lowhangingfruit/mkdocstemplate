# Navigation

In a default installation the navigation is configured in `mkdocs.yml`.

The creator of the [Awesome Pages Plugin](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin) Lukas Geiter, follows the software design paradigm "Convention over Configuration"[^1]. Instead of configuring the navigation in your mkdocs configuration file, this plugin reads markdown files recursively and creates the navigation structure.

[^1]: "Convention over Configuration": Attempts to decrease the number of decisions that a developer using the framework is required to make without necessarily losing flexibility.

Although most of the functionality works with the default settings there are situations where you need more flexibility. The plugin provides this flexibility by using a directory specific configuration file (`.pages`).

The full documentation can be found on the [GitHub page](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin) but to get started here some of my findings.

## Collapse Single Nested Pages

If you have directories that only contain a single page, awesome-pages can "collapse" them, so the folder doesn't show up in the navigation (by default this feature is turned off).

```yaml
collapse_single_pages: false
```

You can set this feature in a directory (`.pages`) or globally in `mkdocsd.yml`.

I played around with it and I stick to the default. You may have a single file in a directory but you probably end up having more files there otherwise you would not have created the directory in the first palce. Makes sense, doesn't it?

## Hide directory

Create a YAML file named `.pages` in a directory and set the hide attribute to true to hide the directory, including all sub-pages and sub-sections, from the navigation:

```yaml
hide: true
```

!!! note Note
    This option only hides the section from the navigation. It will still be included in the build and can be accessed under its URL.

## Set Directory Title

Create a YAML file named `.pages` in a directory and set the title to override the title of that directory in the navigation:

```yaml
title: Page Title
```

## Arrange Pages

Create a YAML file named `.pages` in a directory and set the arrange attribute to change the order of how child pages appear in the navigation. This works for actual pages as well as subdirectories.

```yaml
title: Page Title
arrange:
    - page1.md
    - page2.md
    - subdirectory
```

If you only specify some pages, they will be positioned at the beginning, followed by the other pages in their original order.

You may also include a ... entry at some position to specify where the rest of the pages should be inserted:

```yaml
arrange:
    - index.md
    - ...
    - styling.md
```

This example is from the current situation. 'introduction.md' is positioned at the beginning, styling.md at the end, and the rest of the pages are alphabetically in between.
