# Introduction

This site uses MkDocs and is hosted on [GitLab Pages](https://pages.gitlab.io). You can
[browse the source code](https://gitlab.com/lowhangingfruit/mkdocstemplate), fork it and start using it on your own projects.

## Features

The [MkDocs](http://mkdocs.org) base is good, with the [Material theme](https://squidfunk.github.io/mkdocs-material/) and with some additional plugins it becomes even better.

### The additional plugins:

- [Awesome Pages Plugin](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin), to separate navigation from the configuration. Navigation is now based on the content of the `docs` directory following the "Convention over Configuration" principle.
For more information see the [Navigation](navigation.md) page.
- [PyEmbed-Markdown](https://github.com/pyembed/pyembed-markdown), for embedding content using OEmbed.
TODO: samples page
- [MkDocs Git Revision-Date Plugin](https://github.com/zhaoterryy/mkdocs-git-revision-date-plugin), for setting revision date from git per markdown file.
TODO: samples page
- [PlantUML plugin for Python-Markdown](https://github.com/mikitex70/plantuml-markdown), to use PlantUML diagrams in your documentation.
For more information see the [PlantUML](plantuml.md) page.

### Extensions

Extensive documentation is available from Squidfunk's [MarkDown Extensions](https://squidfunk.github.io/mkdocs-material/extensions/pymdown/) page.
For a brief introduction on how to use the extensions see the [Styling samples](styling.md) page.

## Missing Features

Functionality I would like to have in the Awesome Pages plugin:

- [ ] Sort the pages reversed alphabetically, latest on top.
- [ ] Sort based on a date prefix in a directory

## Nice-to-haves

Looking at other sites like the site from [Alinex](https://alinex.gitlab.io/env/mkdocs/) or [3os](https://3os.org), I see some nice eyecandy but also very usefull stuff.

## Project layout

```nohighlight
mkdocs.yml    # The configuration file.
docs/
    index.md  # The documentation homepage.
    ...       # Other markdown pages, images and other files.
```

## Local testing

To view the MkDocs site locally including plugins you have two options:

- Install MkDocs, install Material for MkDocs and install the used plugins or
- Use the container [`tisgoud/mkdocs-material-plus`]((https://cloud.docker.com/repository/docker/tisgoud/mkdocs-material-plus/general)) that has all the plugins installed.

I prefer the container solution.

### Run MkDocs in a container :whale:

I use Docker Desktop on my Mac to run the MkDocs site on my machine.
The Docker image mounts the `docs` directory from the local MkDocs directory as a volume.

For convenience I created a bash script (named 'serve') to run it.
It's located in the root of the MkDocs directory and needs execution rights (`chmod +x serve`).

### Content of the `serve` Bash script

```bash
#!/usr/bin/env bash

EXPOSED_PORT=8000
echo 'Connect your browser to 127.0.0.1:'${EXPOSED_PORT}

docker run --rm --name mkdocs-material -it \
        -p ${EXPOSED_PORT}:8000 \
        -v ${PWD}:/docs:delegated \
        -v ${PWD}/site:/site:consistent \
        tisgoud/mkdocs-material-plus \
```

### Running `serve`

Running the script:

```bash
$ ./serve
Connect your browser to 127.0.0.1:8000
INFO    -  Building documentation...
INFO    -  Cleaning site directory
[I 190908 20:02:47 server:296] Serving on http://0.0.0.0:8000
[I 190908 20:02:47 handlers:62] Start watching changes
[I 190908 20:02:47 handlers:64] Start detecting changes
```

You can stop the script with ++ctrl+c++

## Deploy to GitLab

A 'git push' triggers the GitLab CI runner. The runner uses an alpine container with python as base. Several plugins and the theme are loaded to 'build' the MkDocs-site.

GitLab runner file `.gitlab-ci.yml`:

```yaml
image: python:alpine

before_script:
  - apk add git
  - pip install mkdocs
  # Add your custom theme if not inside a theme_dir
  # (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
  - pip install mkdocs-material
  # Add your plugins
  - pip install pygments
  - pip install pymdown-extensions
  - pip install mkdocs-awesome-pages-plugin
  - pip install plantuml-markdown

pages:
  script:
    - mkdocs build
    - mv site public
  artifacts:
    paths:
      - public
  only:
    - master
```

## Deployment flows

The diagram below describes the deployment process and local workflow:

```plantuml format="svg"
skinparam monochrome false
skinparam shadowing false
skinparam handwritten false
skinparam boxpadding 40
skinparam roundcorner 10
skinparam ArrowColor black

skinparam ReferenceBorderThickness 10

skinparam actor {
  BorderColor black
}

skinparam control {
  BorderColor black
}

skinparam sequence {
  BoxBorderColor DimGray
  LifeLineBorderColor black
  LifeLineBackgroundColor #FEFFC3
  LifeLineBorderThickness 2
  MessageAlign left
}

title MkDocs workflow; write/test local and deploy to GitLab

actor "MkDocs writer" as writer<< person >> order 10
box "Local machine" #lightBlue
  database "MkDocs" as dir << directory >> order 20
  control "webServer" as lws << process >> order 30
end box

box "GitLab" #orange
  database "master branch" as master << repository >> order 50
  control "runner" as runner << process >> order 60
  database "public dir" as public << webserver >> order 70
end box

writer ->> lws : start local webserver ("./serve")
activate lws

loop write documentation
writer -> dir : modify content
activate dir
lws -->> dir
dir -->> lws
lws ->> lws : processing\nchanges\nin MkDocs dir
writer -> lws : view MkDocs on localhost:8000
end
destroy dir

writer ->> lws : stop local webserver ("control-c")
destroy lws
|||
writer ->> dir : git add .
writer ->> dir : git commit -m "update documentation"
writer ->> dir : git push
dir ->> master : push changes to the master branch
master ->> runner : trigger
activate runner
runner ->> runner : processing
|||
runner -> public : publish
destroy runner

writer -> public : view published documentation

public ->>] : published on internet
```

Last revision: {{ git_revision_date }}
