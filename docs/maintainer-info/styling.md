# Styling samples

The MkDocs-Material theme uses several extension, on this page you can find what they do and how to use them in your markdown. [Extensions documentation](https://squidfunk.github.io/mkdocs-material/extensions/pymdown/)

## Quick Reference table

| Format              | Example                       | Result                                    |
| ------------------- | ----------------------------- | ----------------------------------------- |
| Italic              | `_Italic_` or `*Italic*`      | _Italic_                                  |
| Bold                | `__Bold__` or `**Bold**`      | **Bold**                                  |
| Superscript         | `H^2^O`                       | H^2^O                                     |
| Subscript           | `CH~3~CH~2~OH`                | CH~3~CH~2~OH                              |
| Insert              | `^^Insert^^`                  | ^^Insert^^                                |
| Delete              | `~~Delete me~~`               | ~~Delete me~~                             |
| Mark                | `==mark me==`                 | ==mark me==                               |
| Emoji               | `:smile:`                     | :smile:                                   |
| Code                | \`echo "Hello"\`              | `echo "Hello"`                            |
| Code + Highlighting | \`:::bash echo "Hello"\`      | `:::bash echo "Hello"`                    |
| Keys                | `++ctrl+alt+h++`              | ++ctrl+alt+h++                            |
| Link                | `[Text](http://my-site.com)`  | [Text](http://my-site.com)                |

## Admonitions

!!! abstract "Admonition"
    Admonitions are specially marked "topics" that can appear anywhere an ordinary body element can. They contain arbitrary body elements.
    Typically, an admonition is rendered as an offset block in a document, sometimes outlined or shaded, with a title matching the admonition type.

```reStructuredText
!!! abstract "Admonition"
    Admonitions are specially marked "topics" that can appear anywhere an ordinary body element can. They contain arbitrary body elements.
    Typically, an admonition is rendered as an offset block in a document, sometimes outlined or shaded, with a title matching the admonition type.
```

!!! abstract "abstract, summary, tldr"

!!! bug "bug"

!!! danger "danger, error"

!!! example "example, snippet"

!!! failure "failure, fail, missing"

!!! info "info, todo"

!!! note "note, seealso"

!!! question "question, help, faq"

!!! quote "quote, cite"

!!! success "success, check, done"

!!! tip "tip, hint, important"

!!! warning "warning, caution, attention"

### Advanced examples

???+ example "foldable admonition"
    foldable admonition thanks to the extension 'pymdownx.details'. Instead of '!' use '?'.
    Use a '+' character right after the questionmarks to have it expanded by default.

```reStructuredText
??? example "foldable admonition"
    foldable admonition thanks to the extension 'pymdownx.details'. Instead of '!' use '?'.
    Use a '+' character right after the questionmarks to have it expanded by default.
```

???+ example "foldable and tabbed"
    ```yaml tab="Wrong way"
      apache:
        startservers: 2
        maxclients: 2
    ```

    ```yaml tab="Preferred way"
      apache_startservers: 2
      apache_maxclients: 250
    ```

```reStructuredText
??? example "foldable and tabbed"
    ```yaml tab="Wrong way"
      apache:
        startservers: 2
        maxclients: 2
    ```

    ```yaml tab="Preferred way"
      apache_startservers: 2
      apache_maxclients: 250
    ```
```

???+ example "foldable and nested"
    !!! caution "Complex nested structure"
        ```yaml
          apache:
            startservers: 2
            maxclients: 2
        ```

    !!! hint "Simple flat structure"
        ```yaml
          apache_startservers: 2
          apache_maxclients: 250
        ```

```reStructuredText
??? example "foldable and nested"
    !!! caution "Complex nested structure"
        ```yaml
          apache:
            startservers: 2
            maxclients: 2
        ```

    !!! hint "Simple flat structure"
        ```yaml
          apache_startservers: 2
          apache_maxclients: 250
        ```
```

## Emoji support

The current configuration uses [GitHub's open source emoji solution](https://github.com/github/gemoji), it converts emojis to .png images.

| Emoji   | Syntax   | Name    |
| :------ | :------- | :------ |
| :smile: | \:smile: | Smile   |
| :link:  | \:link:  | Link    |
| :bulb:  | \:bulb:  | Bulb    |
| :bike:  | \:bike:  | Bike    |
| :hugs:  | \:hugs:  | Hugging |

[EmojiCopy](https://www.emojicopy.com/)
[Emojipedia](https://emojipedia.org/)
[Emoji List v12.0](https://unicode.org/emoji/charts/emoji-list.html)

Emoji through single Unicode &#x1f60a; (`&#x1f60a;`)

Person playing waterpolo, multi Unicode: &#x1F93D;&#x200D;&#x2642;&#xFE0F; (`&#x1F93D;&#x200D;&#x2642;&#xFE0F;`)

Person playing waterpolo with medium-light skin tone: &#x1F93D;&#x200D;&#x2642;&#xFE0F;&#x1F3FC; (`&#x1F93D;&#x200D;&#x2642;&#xFE0F;&#x1F3FC;`)

## Sane lists

Lists can start with another number than 1

4. third item
5. fourth item
6. fifth item

## Code block

Use three back-ticks to start and end the code block.

````markdown
```json
{
  "@type": "ResourceActionRequest",
  "id": "9e89ebfc-8736-4980-a026-ade761255295",
  "iconId": "5fccca72-4b09-41b6-a216-da10f388e352_icon",
  "version": 5,
  "requestNumber": 740489,
  "state": "SUCCESSFUL",
  "description": "Called via API",
  "reasons": null,
  "requestedFor": "tenant",
  "requestedBy": "tenant,
  "organization": {
    "tenantRef": "TenantName",
    "tenantLabel": "TenantName",
    "subtenantRef": "50b0bc4b-a1c5-42a9-9288-e035ea8407ba",
    "subtenantLabel": "TenantName"
  },
  …
```
````

### Highlight line numbers

Specific lines can be highlighted by passing the line numbers to the hl_lines argument placed right after the language identifier.

````markdown
```python hl_lines="3 4"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```
````

```python hl_lines="3 4"
""" Bubble sort """
def bubble_sort(items):
    for i in range(len(items)):
        for j in range(len(items) - 1 - i):
            if items[j] > items[j + 1]:
                items[j], items[j + 1] = items[j + 1], items[j]
```

Inline code looks like `inline code` when you surround the text with "back-ticks".

## Keys

A key or combination of key presses is surrounded by `++` with each key press separated with a single `+`.

\+\+ctrl+alt+del\+\+
++ctrl+alt+del++

## Mark

Mark ==some text== by using \=\=around the text\=\=

## SmartSymbols

SmartSymbols converts markup for special characters.

| Markdown          | Result        |
| :---------------- | :------------ |
| `(tm)`            | (tm)          |
| `(c\)`            | (c)           |
| `(r\)`            | (r)           |
| `+/-`             | +/-           |
| `-->`             | -->           |
| `<--`             | <--           |
| `<-->`            | <-->          |
| `=/=`             | =/=           |
| `1/4`, etc.       | 1/4, etc      |
| `1st`, `2nd` etc. | 1st, 2nd etc. |

## Tasklist

Checkbox style lists:

- [x] One item
- [ ] and another
    - [x] another
    - [ ] even more
- [x] and now it's enough

```markdown
- [x] One item
- [ ] and another
    - [x] another
    - [ ] even more
- [x] and now it's enough
```

## Tilde

You can use \~\~[text]\~\~ to ~~"strike-through"~~ text.

## Tables

| Header 1  | Header 2  |
| :-------: | :-------: |
| value 1-1 | value 1-2 |
| value 2-2 | value 2-2 |

```markdown
| Header 1  | Header 2  |
| :-------: | :-------: |
| value 1-1 | value 1-2 |
| value 2-2 | value 2-2 |
```

## Resizing images

`![alt text](image-url){: style="height:150px;width:150px"}`

## Footnotes

Footnotes[^1] have a label[^@#$%] and the footnote's content.

[^1]: This is a footnote content.
[^@#$%]: A footnote on the label: "@#$%".

```nohighlight
Footnotes[^1] have a label[^@#$%] and the footnote's content.

[^1]: This is a footnote content.
[^@#$%]: A footnote on the label: "@#$%".
```
