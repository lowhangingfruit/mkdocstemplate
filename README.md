![Build Status](https://gitlab.com/pages/mkdocs/badges/master/build.svg)

---

# MkDocsTemplate

[![screenshot of MkDocsTemplate](docs/assets/images/screenshot.png)](https://lowhangingfruit.gitlab.io/mkdocstemplate)

[MkDocsTemplate in GitLab pages](https://lowhangingfruit.gitlab.io/mkdocstemplate)

## Why

### The initial challenge

I started working with a very basic MkDocs installation. After a while I chose a new theme, added plugins, added extensions and started tweaking it.

Making the no-longer basic installation re-usable was the main reason to create this repo.

### The goal

Provide a default -_customized_- documentation template based on MKDocs/markdown which you can quickly start creating your own documentation pages.

## What

Personalized [template](https://lowhangingfruit.gitlab.io/mkdocstemplate) of the sample [MkDocs](http://www.mkdocs.org) website using [GitLab Pages](https://pages.gitlab.io).

The customizations are still work-in-progress, you can read more about them in the ["Maitainer section"](https://lowhangingfruit.gitlab.io/mkdocstemplate/maintainer-info/introduction/).

## How

The diagram below describes the local workflow and the deployment proces:

![Workflow](docs/assets/images/workflow.svg)

### Deployment (GitLab CI)

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/), following the steps defined in `.gitlab-ci.yml`:

```yaml
image: python:alpine

before_script:
  - apk add git
  - pip install mkdocs
  # Add your custom theme if not inside a theme_dir
  # (https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)
  - pip install mkdocs-material
  # Add your plugins
  - pip install pygments
  - pip install pymdown-extensions
  - pip install mkdocs-awesome-pages-plugin
  - pip install pyembed-markdown
  - pip install mkdocs-git-revision-date-plugin
  - pip install plantuml-markdown

pages:
  script:
    - mkdocs build
    - mv site public
  artifacts:
    paths:
      - public
  only:
    - master
```

### Deployment local

To view this site locally you have two options:

- Install MkDocs, install Material for MkDocs and install the used plugins or
- Use the container [`tisgoud/mkdocs-material-plus`]((https://cloud.docker.com/repository/docker/tisgoud/mkdocs-material-plus/general)) that has all the plugins installed.

I prefer the container solution.

#### Run MkDocs in a container :whale:

I use Docker Desktop on my Mac to run the MkDocs site on my machine.
The Docker image mounts the `docs` directory from the local MkDocs directory as a volume.

For convenience I created a bash script (named 'serve') to run it.
It's located in the root of the MkDocs directory and needs execution rights (`chmod +x serve`).

#### Content of the `serve` Bash script

```bash
#!/usr/bin/env bash

EXPOSED_PORT=8000
echo 'Connect your browser to 127.0.0.1:'${EXPOSED_PORT}

docker run --rm --name mkdocs-material -it \
        -p ${EXPOSED_PORT}:8000 \
        -v ${PWD}:/docs:delegated \
        -v ${PWD}/site:/site:consistent \
        tisgoud/mkdocs-material-plus \
```

#### Running `serve`

Running the script:

```bash
$ ./serve
Connect your browser to 127.0.0.1:8000
INFO    -  Building documentation...
INFO    -  Cleaning site directory
[I 190908 20:02:47 server:296] Serving on http://0.0.0.0:8000
[I 190908 20:02:47 handlers:62] Start watching changes
[I 190908 20:02:47 handlers:64] Start detecting changes
```

You can stop the script with "ctrl+c".

---

Forked from [https://gitlab.com/morph027/mkdocs](https://gitlab.com/morph027/mkdocs)
